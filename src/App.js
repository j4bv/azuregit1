import React, { Component } from 'react';
import {
  Container, Col, Form,
  FormGroup, Label, Input,
  Button, FormText, FormFeedback,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  CustomInput,
  Alert,
  DropdownItem
} from 'reactstrap';
import './App.css';
import logoBMW from './logo.png'

const request = require('request')
const AzureApi = 'https://prod-50.eastus.logic.azure.com:443/workflows/38247ab16a814a17bf189c3fe778a728/triggers/manual/paths/invoke?api-version=2016-10-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=fzV0Kjn1jzsv6TwXR9x6dka2erJLsCa_bl8P59_ei60'

class App extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = { fadeIn: true };
      this.state = {
      isOpen: false,
      isSuccesful: false,
      isFine: true,
      'email': '',
      'password': '',
      validate: {
        emailState: '',
      },
    }
    this.onDismiss = this.onDismiss.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  validateEmail(e) {
    const emailRex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const { validate } = this.state
      if (emailRex.test(e.target.value)) {
        validate.emailState = 'has-success'
        this.setState({
          isFine: false
        });
      } else {
        validate.emailState = 'has-danger'
        this.setState({
          isFine: true
        });
      }
      this.setState({ validate })
    }

  handleChange = async (event) => {
    const { target } = event;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const { name } = target;
    await this.setState({
      [ name ]: value,
    });
  }

  submitForm(e) {
    e.preventDefault();
    window.scrollTo(0, 0)
    console.log(`Email: ${ this.state.email }`)
    //alert("Submitted")

   // window.alert(JSON.stringify(this.state, 0, 2))
  request.post(AzureApi,{
    json: {
            "input": {
                        "data": this.state.email,
                        "timestamp": this.state.password
                    }
           
        }
    }, (error, res, body) => {
        if (error){
            console.error(error)
            return
        }
        console.log(`statusCode: ${res.statusCode}`)
        this.setState({
          isSuccesful: true
        });
        console.log(body)
    }
)
  }


  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }

  onDismiss() {
    this.setState({ isSuccesful: false });
  }

  render() {
    const { email, password, isFine } = this.state;

    return (
      <div>
         <Navbar color="light" light expand="md">
         <img src={logoBMW} className="App-logo" alt="logo" width="20" />
          <NavbarBrand href="/">&ensp;BMW | RPA Ideas Collection  </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="/components/">Components</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://github.com/reactstrap/reactstrap">GitHub</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  Options
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Option 1
                  </DropdownItem>
                  <DropdownItem>
                    Option 2
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    Reset
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      <Container className="App">
      <Alert color="success" isOpen={this.state.isSuccesful} toggle={this.onDismiss} fade={true}>
          Your data was sent succesfully!
      </Alert>
      <center><h2>New ideas Forn</h2></center>
      <p className="text-muted">  Please fill out the next form in order to continue with the process.</p>

        <Form className="form" onSubmit={ (e) => this.submitForm(e) }>
          <Col>
            <FormGroup>
              <Label>Username</Label>
              <Input
                type="email"
                name="email"
                id="exampleEmail"
                placeholder="myemail@email.com"
                value={ email }
                valid={ this.state.validate.emailState === 'has-success' }
                invalid={ this.state.validate.emailState === 'has-danger' }
                onChange={ (e) => {
                            this.validateEmail(e)
                            this.handleChange(e)
                          } }
              />
              <FormFeedback valid>
                That's a tasty looking email you've got there.
              </FormFeedback>
              <FormFeedback>
                Uh oh! Looks like there is an issue with your email. Please input a correct email.
              </FormFeedback>
              <FormText>Your username is most likely your email.</FormText>
            </FormGroup>
          </Col>
          <Col>
            <FormGroup>
              <Label for="examplePassword">Password</Label>
              <Input
                type="password"
                name="password"
                id="examplePassword"
                placeholder="********"
                value={ password }
                onChange={ (e) => this.handleChange(e) }
            />
            </FormGroup>
          </Col>
          <Col> 
          <FormGroup>
          <Label for="exampleCheckbox">Radios</Label>
          <div>
            <CustomInput type="radio" id="exampleCustomRadio" name="customRadio" label="Select this custom radio"/>
            <CustomInput type="radio" id="exampleCustomRadio2" name="customRadio" label="Or this one" />
            <CustomInput type="radio" id="exampleCustomRadio3" label="But not this disabled one" disabled />
            <CustomInput type="radio" id="exampleCustomRadio4" label="Can't click this label to select!" htmlFor="exampleCustomRadio4_X" disabled />
          </div>
        </FormGroup>
        </Col>
        <Col>
        <FormGroup>
          <Label for="exampleNumber">Number</Label>
          <Input
            type="number"
            name="number"
            id="exampleNumber"
            min="0"
            step=".01"
            placeholder="0.00"
          />
        </FormGroup>
        </Col>

          <Button disabled={isFine}>Submit</Button>
      </Form>
      </Container>
      
      </div>
    );
  }
}

export default App;